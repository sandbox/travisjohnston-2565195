// JavaScript Document

CKEDITOR.dialog.add( 'wysiwyg_flexDialog', function( editor ) {
    return {
        title: 'Flex Container Properties',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Flex Container Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'columns',
                        label: 'Columns',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Columns field cannot be empty." )
                    },
                    {
                        type: 'text',
                        id: 'rows',
                        label: 'Rows',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Rows field cannot be empty." )
                    },
					{
						type: 'select',
						id: 'grid-style',
						label: 'Choose how you would like to layout the Flex Table',
						items:
						[
							['Even (default - each column has an evenly displaced width)','even'],
							['Large First Row', 'large_first'],
							['Large Last Row', 'large_last']
						],
						style: '','default': 'even'
					}
                ]
            },
        ],
        onOk: function() {
            var dialog = this;
			var columns = dialog.getValueOf( 'tab-basic', 'columns' );
			var rows = dialog.getValueOf( 'tab-basic', 'rows' );
			var layout = dialog.getValueOf( 'tab-basic', 'grid-style' ); 			
			var flexRows = '<div class="flex-container">';
			var colWidth = (100/columns)-5 + '%';
			var largeColWidth = '45%';
			var remainingColsWidth = (50/columns) + '%';
			function createFlexs (rows, columns, layout){
			  for (var i=0 ;i < rows ;i++){
				   flexRows += '<div class="flex-row">';
				   		switch(layout){
							case 'even':
								for (var e=0 ;e < columns ;e++){								
										flexRows += '<div class="flex-col" style="float:left; width:';
										flexRows += colWidth;
										flexRows += ';">Text Here</div>';
								}
							break;
							case 'large_first':
										flexRows += '<div class="flex-col" style="float:left; width:';
										flexRows += largeColWidth;
										flexRows += ';">Large Text Here</div>';
								for (var l=1 ;l < columns ;l++){
										flexRows += '<div class="flex-col" style="float:left; width:';
										flexRows += remainingColsWidth;
										flexRows += ';">Text Here</div>';
								}
							break;
							case 'large_last':
								for (var l2=1 ;l2 < columns ;l2++){
										flexRows += '<div class="flex-col" style="float:left; width:';
										flexRows += remainingColsWidth;
										flexRows += ';">Text Here</div>';
								}
										flexRows += '<div class="flex-col" style="float:left; width:';
										flexRows += largeColWidth;
										flexRows += ';">Large Text Here</div>';
							break;
						}
				   flexRows += '</div>';
				}
			}
			createFlexs(rows, columns, layout);
			flexRows += '</div>';
			editor.insertHtml(flexRows);
        }
    };
});