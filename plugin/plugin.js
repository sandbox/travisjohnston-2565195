// JavaScript Document

(function($) {
 CKEDITOR.plugins.add( 'wysiwyg_flex', {
  init: function( editor )
  {
  
   // now time for some magic		
   editor.addCommand( 'wysiwyg_flex', new CKEDITOR.dialogCommand( 'wysiwyg_flexDialog' ) );
   
   editor.ui.addButton( 'wysiwyg_flex_button', {
    label: 'Flex Container', //this is the tooltip text for the button
    command: 'wysiwyg_flex',
	toolbar: 'insert',
	icon: this.path + 'images/icon.gif'
   });
   
   CKEDITOR.dialog.add( 'wysiwyg_flexDialog', this.path + 'dialogs/dialog.js' );
  }
 });
})(jQuery);