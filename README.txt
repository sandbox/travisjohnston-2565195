-- SUMMARY --

WYSIWYG Responsive Table module is a simple plugin that adds the ability to include a button in your WYSIWYG toolbar that inserts a responsive table that content editors can use without needing to mess with the editor's source code. 

-- REQUIREMENTS --

 - You need to have the WYSIWYG Module installed with the CKE Library. More info can be found on the WYSIWYG module's page https://www.drupal.org/project/wysiwyg and the CKE Library can be found at http://ckeditor.com/download


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* After installing, go to admin/config/content/wysiwyg and edit the WYSIWYG profile that you want to add the button to. Under Buttons and Plugins click the checkbox next to Add a custom Flex Container Table


-- TROUBLESHOOTING --

* If the button does not display in the editor, try clearing cache. 

-- FAQ --

Q: Any new features?

A: This is a simple module at the moment, only taking columns and rows as options. I will be adding the ability to set more defined properties such as colwidth (preset of course) and padding. This is my first contributed module so I am getting the basics set first.


-- CONTACT --

Current maintainers:
* Travis Johnston - https://www.drupal.org/user/1730656
